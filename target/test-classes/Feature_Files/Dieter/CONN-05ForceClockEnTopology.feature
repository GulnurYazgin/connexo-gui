@Run
Feature: Start an Adhoc Force Clock E to sync the clock and Topology to make the G-Meter visible

  Scenario Outline: 01 Successfully run the communication profile Force Clock and Topology
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user sees "Communication tasks" page
    And user clicks on the action button of the communication task "Ad Hoc Force Clock E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Force Clock E" has the status "Waiting"
    And user clicks on "Overview" from the sub menu
    And user sees "Overview" page
    And user verifies if connection method "Ad Hoc ASAP" has the last connection "Successful" and the status "Active"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Topology E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Topology E" has the status "Waiting"
    And user clicks on "Overview" from the sub menu
    And user sees "Overview" page
    And user verifies if connection method "Ad Hoc ASAP" has the last connection "Successful" and the status "Active"
    Examples:
      | DeviceNo          |
      | E0058000704680519 |
#  here we have a visual check. we can only check it via connexxo. this will be discussed (to find the most reliable way of checking)

