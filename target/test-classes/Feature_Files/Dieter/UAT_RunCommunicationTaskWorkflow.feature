@Run
Feature: Run Communication Task Workflow

  Scenario Outline: 01 Successfully run communication task workflow
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    When user clicks on the "Action" button and selects "Start process"
    Then user sees "Start process" page
    And user selects "Run communication task workflow (1.0)" from the "Process" dropdown "1"
    When user clicks on "Start"
    Then user enters the following values
      | field                                                      | value                |
      | Communication task name                                    | Ad Hoc Force Clock E |
      | Estimated duration of com task execution in minutes        | 4                    |
      | Max duration of check result subworkflow (format #d#h#m#s) | 2m                   |
    And user clicks on "Start"
    When user sees "Processes" page
    Then user waits until there are no running "Run communication task workflow" process
    And user clicks "History" button of the Process Page
    Then user verifies if latest Process "Run communication task workflow" is completed and has the following results
      | variable | result    |
      | Status   | Completed |
#  Its required to check if the workflow is succesful.(Status overview&nodes)
    Examples:
      | DeviceNo          |
      | SIM_E000000019619 |

  Scenario Outline: 02 Successfully run communication task workflow via Communication tasks
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "<ComTaskName>"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Force Clock E" has the status "Waiting"
    Examples:
      | DeviceNo          |ComTaskName|
      | SIM_E000000019619 |Ad Hoc Force Clock E|


