@Run
Feature:Successfully send Calendar

  Scenario Outline: 02 Successfully send Calendar via Configuration submenu
  #    Calendar is filled in manually. we want it to be smart and shuffle between 10 and 30 as necessary.
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Read Registers Configuration E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers Configuration E" has the status "Waiting"
    And user clicks on "Overview" from the sub menu
    And user sees "Overview" page
    And user verifies if connection method "Ad Hoc ASAP" has the last connection "Successful" and the status "Active"
    Then user clicks on "Configuration" from the sub menu
    And user clicks on "Time of use" from the sub menu
    When user clicks on the "Actions" button and selects "Send calendar"
    Then user selects "30" from the "Time of use calendar" dropdown "1"
    And user selects radio button number "2" from the "Release date (command)" field
    And user selects the timestamp which is -1 hour from the actual time
    And user clicks on "Save"
    And user saves the "Release date (command)" time
    And user clicks on "Commands" from the sub menu
    Then user selects "Trigger now" from the command action button
    And user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Read Registers Configuration E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers Configuration E" has the status "Waiting"
    And user clicks on "Overview" from the sub menu
    And user sees "Overview" page
    And user verifies if connection method "Ad Hoc ASAP" has the last connection "Successful" and the status "Active"
    And user clicks on "Time of use" from the sub menu
    Then user verifies that the "Time of use calendar" is "30"
  ##    is there a way to see which telwerk is active on the meter at the moment?
    Examples:
      | DeviceNo          |
      | E0058000704680519 |