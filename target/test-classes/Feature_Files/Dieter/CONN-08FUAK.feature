@Run
Feature: Start a FUAK change

  Scenario Outline: 01 Successfully send FUAK
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Read Registers Configuration G"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers Configuration G" has the status "Waiting"
    Then user clicks on "Data sources" from the sub menu
    And user clicks on "Registers" from the sub menu
    Then user verifies if register "02 Mbus Encryption status (Code)" has the value "4"
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "Security" from the "Command category" dropdown "1"
    Then user selects "Set M-Bus Firmware Update Authentication Key (FUAK)" from the "Command" dropdown "1"
    And user saves the release date
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Read Registers Configuration G"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers Configuration G" has the status "Waiting"
    And user clicks on "Registers" from the sub menu
    Then user verifies if register "02 FUAK status (Code)" has the value "1"
    Examples:
      | DeviceNo          |
      | G0056003610005917 |

  Scenario Outline: 02 Verify FUAK value
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Read Registers Configuration G"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers Configuration G" has the status "Waiting"
    Then user clicks on "Data sources" from the sub menu
    And user clicks on "Registers" from the sub menu
    Then user verifies if register "02 FUAK status (Code)" has the value "2"
    Examples:
      | DeviceNo          |
      | G0056003610005917 |