@Run
Feature: Start Key renewal process

  Scenario Outline: 01 Start Key renewal process
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    When user clicks on the "Action" button and selects "Start process"
    Then user sees "Start process" page
    And user selects "Key renewal" from the "Process" dropdown "1"
    When user clicks on "Start"
    Then user enters the following values
      | field                                                      | value |
      | Security accessor                                          | AK    |
      | Estimated duration of com task execution in minutes        | 4     |
      | Max duration of check result subworkflow (format #d#h#m#s) | 2m    |
    And user checks "No retries needed" checkbox on
    And user clicks on "Start"
    When user sees "Processes" page
    Then user waits until there are no running "Key renewal" process
    And user clicks "History" button of the Process Page
    Then user verifies if latest Process "Key renewal" is completed and has the following results
      | variable | result    |
      | Status   | Completed |
    Examples:
      | DeviceNo          |
      | SIM_E000000019619 |

