@Run
Feature: Manual Firmware Upgrade feature

  Scenario Outline: 01 Successfully upgrade and verify a firmware upgrade
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Configuration" from the sub menu
    And user clicks on "Firmware" from the sub menu
    Then user clicks on the action button of the firmware type "Device firmware"
    And user clicks on option "Check firmware version/image now"
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    Then user waits until the communication task "Ad Hoc Read Registers Configuration E" has the status "Waiting"
    And user clicks on "Firmware" from the sub menu
    Then user reads and prints the "Firmware version" value
    And user confirms that the actual firmware version of the meter is not the same as "<FirmwareVersion>"
    Then user clicks on the action button of the firmware type "Device firmware"
    And user clicks on option "Upload firmware/image and activate immediately"
    And user selects "<FirmwareVersion>" from the "Firmware file" dropdown "1"
    And user clicks on "Confirm"
    And user clicks on "Upload"
    Then user saves the starting time of the firmware upgrade
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    Then user waits until the communication task "Firmware management" has the status "Waiting"
    And user clicks on the action button of the communication task "Ad Hoc Read Registers Configuration E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers Configuration E" has the status "Waiting"
    Then user clicks on "Data sources" from the sub menu
    And user clicks on "Registers" from the sub menu
    Then user selects "200" from the "Registers per page" dropdown
    And user verifies if register "01 FW Upgrade image transfer status (Code)" has the value "6"
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Events E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Events E" has the status "Waiting"
    Then user clicks on "Data sources" from the sub menu
    And user clicks on "Events" from the sub menu
    Then user clicks on the dropdown number "1" and selects "Firmware"
    And user clicks on "Apply"
    Then user verifies that event "17" is visible with the correct timestamp
    Then user verifies that event "18" is visible with the correct timestamp
    Then user verifies that event "20" is visible with the correct timestamp
    Examples:
      | DeviceNo          |FirmwareVersion |
      | E0058000704680519 |1.2.0021 (Final)|

#  Then user checks the events for verification
#  And user checks the firmware version information in the firmware tab

#    NOTE: Campaign is necessary to upgrade to the same firmware version

