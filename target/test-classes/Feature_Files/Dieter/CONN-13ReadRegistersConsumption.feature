@Run
Feature: Send the Read Registers Consumption communication profile

  Scenario Outline: 01 Successfully send the Read Registers Consumption communication profile
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad HocP5 Read Registers Meterread Actual E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad HocP5 Read Registers Meterread Actual E" has the status "Waiting"
    Then user clicks on "Data sources" from the sub menu
    And user clicks on "Registers" from the sub menu
    Then user selects "200" from the "Registers per page" dropdown
    Then user checks the "Secondary Bulk 03 Active Energy Export - Rate1 (kWh) ToU 1" register and prints the value
    Then user checks the "Secondary Bulk 03 Active Energy Export - Rate2 (kWh) ToU 2" register and prints the value
    Then user checks the "Secondary Bulk 03 Active Energy Export Total (kWh)" register and prints the value
    Then user checks the "Secondary Bulk 03 Active Energy Import - Rate1 (kWh) ToU 1" register and prints the value
    Then user checks the "Secondary Bulk 03 Active Energy Import - Rate2 (kWh) ToU 2" register and prints the value
    Then user checks the "Secondary Bulk 03 Active Energy Import Total (kWh)" register and prints the value
    Examples:
      | DeviceNo          |
#      | E0058000704680519 |
    |E0058000704679919 |