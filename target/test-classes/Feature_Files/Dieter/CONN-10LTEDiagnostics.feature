@Run
Feature: Send the Read Registers LTE Diagnostics Statistics E communication profile

  Scenario Outline: 01 Successfully send the Read Registers LTE Diagnostics Statistics E communication profile
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Read Registers LTE Diagnostics Statistics E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers LTE Diagnostics Statistics E" has the status "Waiting"
    Then user clicks on "Data sources" from the sub menu
    And user clicks on "Registers" from the sub menu
    Then user clicks on the dropdown number "1" and selects "LTE Diagnostics and Statistics"
    And user clicks on "Apply"
    Then user checks the "09 LTE Monitoring (Code)" register and prints RSRP value in dB
    Examples:
      | DeviceNo          |
      | E0058000704680519 |
