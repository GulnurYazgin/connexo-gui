@Run
Feature: Send Calendar Process

  Scenario: 01 Successfully send Calendar via processes
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "SIM_E000000019619"
    When user clicks "Search" button
    Then user clicks on search result "SIM_E000000019619"
    And user sees "Overview" page
    When user clicks on the "Action" button and selects "Start process"
    Then user sees "Start process" page
    And user selects "Send Calendar" from the "Process" dropdown "1"
    When user clicks on "Start"
    Then user enters the following values
      | field                                                      | value                |
      | Estimated duration of com task execution in minutes        | 4                    |
      | Max duration of check result sub workflow | 2m                   |
    And user clicks on "Start"
    When user sees "Processes" page
    Then user waits until there are no running "Send Calendar" process
    And user clicks "History" button of the Process Page
    Then user verifies if latest Process "Send Calendar" is completed and has the following results
      | variable | result    |
      | Status   | Completed |



