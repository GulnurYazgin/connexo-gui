@Run
Feature: Manual G-Meter Firmware Upgrade feature

  Scenario Outline: 01 Successfully upgrade and verify a G-Meter firmware upgrade for the G-Meter
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    And user clicks on "Data sources" from the sub menu
    And user clicks on "Registers" from the sub menu
    Then user verifies if register "02 FUAK status (Code)" has the value "2"
    Then user clicks on "Configuration" from the sub menu
    And user clicks on "Firmware" from the sub menu
    Then user clicks on the action button of the firmware type "Device firmware"
    And user clicks on option "Check firmware version/image now"
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    Then user waits until the communication task "Ad Hoc Read Registers Configuration G" has the status "Waiting"
    And user clicks on "Firmware" from the sub menu
    Then user reads and prints the "Firmware version" value
    Then user clicks on the action button of the firmware type "Device firmware"
    And user clicks on option "Upload firmware/image and activate immediately"
    And user selects "<FirmwareVersion>" from the "Firmware file" dropdown "1"
    And user clicks on "Confirm"
    And user clicks on "Upload"
    Then user saves the starting time of the firmware upgrade
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    #I need to confirm which com profile this is.
    Then user waits until the communication task "Mass Commands G" has the status "Waiting"
    And user clicks on "Firmware" from the sub menu
    Then user clicks on the action button of the firmware type "Device firmware"
    And user clicks on option "Check firmware version/image now"
    And user clicks on "Communication tasks" from the sub menu
    Then user waits until the communication task "Ad Hoc Read Registers Configuration G" has the status "Waiting"
    And user clicks on "Firmware" from the sub menu
    Then user verifies if "Firmware version" has the value "<FirmwareVersion>"
    And user clicks on "Communication tasks" from the sub menu
    And user sees "Communication tasks" page
    And user clicks on the action button of the communication task "Scheduled Standard Evaluation G"
    And user clicks on option "Run now"
    Then user waits until the communication task "Scheduled Standard Evaluation G" has the status "Waiting"
    And user clicks on "Events" from the sub menu
    Then user clicks on the dropdown number "1" and selects "Firmware"
    And user clicks on "Apply"
#    Then user verifies that event "116" is visible with the correct timestamp
    Examples:
      | DeviceNo          |FirmwareVersion |
      | G0058530042952320 |82 b02.01 (Final)|
