@Run
Feature: Read Mbus Statistics

  Scenario Outline: 01 Successfully read the mbus statistics values
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Read Registers M-Bus Diagnostics Statistics G"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers M-Bus Diagnostics Statistics G" has the status "Waiting"
#    Then user clicks on "Data sources" from the sub menu
#    And user clicks on "Registers" from the sub menu
#    Then user selects "200" from the "Registers per page" dropdown
#    Then user checks the "10 Mbus Statistics Average Missed C (Count)" register and prints the value
#    Then user checks the "10 Mbus Statistics Average Missed T (Count)" register and prints the value
#    Then user checks the "10 Mbus Statistics Number Missed C (Count)" register and prints the value
#    Then user checks the "10 Mbus Statistics Number Missed T (Count)" register and prints the value
    Examples:
      | DeviceNo          |
#      | G0058530042952320 |
  |G0056003610005917    |


