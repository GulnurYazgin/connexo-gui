@Run
Feature: Commission a slave device

  Scenario Outline: 01 Commission a slave device via commands
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "MBus setup" from the "Command category" dropdown "1"
    Then user selects "Change MBus attributes" from the "Command" dropdown "1"
    And user enters the following values
      | field        | value             |
      | Mbus channel | <MbusChannel>     |
    When user clicks on "Yes"
    And user enters the following values
      | field                             | value                            |
      | MBus client identification number | <MBusClientIdentificationNumber> |
      | MBus client manufacturer id       | <MBusClientManufacturerId>       |
      | MBus client version               | <MBusClientVersion>              |
      | MBus client device type           | <MBusClientDeviceType>           |
    And user saves the release date
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Topology E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Topology E" has the status "Waiting"
    And user clicks on "Overview" from the sub menu
    And user verifies that the slave/master device has the value "G0072004011395220"
    Then user clicks on the slave/master device
    And user sees "Overview" page
    And user verifies that the slave/master device has the value "E0061000000062419"
    Examples:
      | DeviceNo          |MbusChannel|MBusClientIdentificationNumber|MBusClientManufacturerId|MBusClientVersion|MBusClientDeviceType|
      | E0061000000062419 |1          |40113952                      |ELS                     |080              |03                  |