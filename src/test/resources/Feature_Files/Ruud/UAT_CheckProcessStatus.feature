@Run
Feature: Check if a process status

  Scenario Outline: Check status of a process case
    Given user logs in to "acc" environment
    When user goes to "Admin"
    Then user sees "Administration" page
    When user selects "Processes" under the "Process management" object
    Then user sees "Processes" page
    #its needed to select the items per page as 200 from dropdown
    #then scroll down to searched process
    And user verifies if process name "<ProcessName>" has the status "<Status>"
    Examples:
      | ProcessName                 |Status|
      | Add calendar on usage point |Active|

