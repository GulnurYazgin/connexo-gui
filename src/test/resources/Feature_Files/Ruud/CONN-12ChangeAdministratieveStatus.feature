@Run
Feature: Send Administrative Status

  Scenario Outline: 01 Succesgit psfully send Administrative status of an E-Meter
#    Calendar is filled in manually. we want it to be smart and shuffle between 10 and 30 as necessary.
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Communication tasks" from the sub menu
    #we dont have right to send this com profile!!!!
    And user clicks on the action button of the communication task "Ad Hoc Read Registers Configuration E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers Configuration E" has the status "Waiting"
    And user clicks on "Data sources" from the sub menu
    And user clicks on "Registers" from the sub menu
    And user checks the "01 Administrative Status (Code)" register and prints the value
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "Configuration change" from the "Command category" dropdown "1"
    Then user selects "Change administrative status" from the "Command" dropdown "1"
    And user selects the new administrative status from the "Administrative status" dropdown "1"
    And user saves the release date
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Read Registers Configuration E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Read Registers Configuration E" has the status "Waiting"
    And user clicks on "Data sources" from the sub menu
    And user clicks on "Registers" from the sub menu
    And user checks the "01 Administrative Status (Code)" register and prints the value
    Then user verifies if register "01 Administrative Status (Code)" has the correct value
    Examples:
      | DeviceNo          |
      | E0058000704680519 |


