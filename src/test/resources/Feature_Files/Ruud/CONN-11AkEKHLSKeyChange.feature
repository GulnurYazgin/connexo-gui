@Run
Feature: Start a AK EK and HLS Key change

  Scenario Outline: 01 Successfully send AK and EK voor een meter
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "AK"
    And user clicks on option "Generate passive key"
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "Security" from the "Command category" dropdown "1"
    Then user selects "Renew key" from the "Command" dropdown "1"
    And user saves the release date
    Then user selects "AK" from the "Key accessor type" dropdown "1"
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "AK"
    And user clicks on option "Activate passive key"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Communication Check E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Communication Check E" has the status "Waiting"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "AK"
    And user clicks on option "Clear passive key"
    And user clicks on the action button of the "EK"
    And user clicks on option "Generate passive key"
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "Security" from the "Command category" dropdown "1"
    Then user selects "Renew key" from the "Command" dropdown "1"
    And user saves the release date
    Then user selects "EK" from the "Key accessor type" dropdown "1"
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "EK"
    And user clicks on option "Activate passive key"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Communication Check E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Communication Check E" has the status "Waiting"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "AK"
    And user clicks on option "Clear passive key"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Scheduled Standard Evaluation E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Scheduled Standard Evaluation E" has the status "Waiting"
    #Then user verifies that there are no weird events on the meter
    Examples:
      | DeviceNo          |
      | E0063001001631919 |

  Scenario Outline: 02 Successfully send AK and EK and HLS voor een meter
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "AK"
    And user clicks on option "Generate passive key"
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "Security" from the "Command category" dropdown "1"
    Then user selects "Renew key" from the "Command" dropdown "1"
    And user saves the release date
    Then user selects "AK" from the "Key accessor type" dropdown "1"
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "AK"
    And user clicks on option "Activate passive key"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Communication Check E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Communication Check E" has the status "Waiting"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "AK"
    And user clicks on option "Clear passive key"
    And user clicks on the action button of the "EK"
    And user clicks on option "Generate passive key"
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "Security" from the "Command category" dropdown "1"
    Then user selects "Renew key" from the "Command" dropdown "1"
    And user saves the release date
    Then user selects "EK" from the "Key accessor type" dropdown "1"
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "EK"
    And user clicks on option "Activate passive key"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Communication Check E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Communication Check E" has the status "Waiting"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "EK"
    And user clicks on option "Clear passive key"
    And user clicks on the action button of the "HLS"
    And user clicks on option "Generate passive key"
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "Security" from the "Command category" dropdown "1"
    Then user selects "Renew key" from the "Command" dropdown "1"
    And user saves the release date
    Then user selects "HLS" from the "Key accessor type" dropdown "1"
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands E" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "HLS"
    And user clicks on option "Activate passive key"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Communication Check E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Communication Check E" has the status "Waiting"
    And user clicks on "Security accessors" from the sub menu
    And user clicks on the action button of the "HLS"
    And user clicks on option "Clear passive key"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Scheduled Standard Evaluation E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Scheduled Standard Evaluation E" has the status "Waiting"
    Examples:
      | DeviceNo          |
      | E0063001001631919 |
    #Then user verifies that there are no weird events on the meter