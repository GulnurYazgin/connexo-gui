@Run
Feature: Start Device Main Key renewal Workflow

  Scenario Outline: 01 Start Device Main Key renewal Workflow
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    When user clicks on the "Action" button and selects "Start process"
    Then user sees "Start process" page
    And user selects "Main key renewal" from the "Process" dropdown "1"
    And user checks "Is device addressable" checkbox on
    Then user enters the following values
      | field                                                      | value |
      | Estimated duration of com task execution in minutes        | 4     |
      | Max duration of check result subworkflow (format #d#h#m#s) | 2m    |
    And user checks "No retries needed" checkbox on
    And user clicks on "Start"
    When user sees "Processes" page
    #checks for the status of the process should be added
    Examples:
      | DeviceNo          |
      | SIM_E000000019619 |


