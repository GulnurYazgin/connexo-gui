@Run
Feature: Send Administrative Status

  Scenario Outline: 01 Successfully change Administrative Status
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    When user clicks on the "Action" button and selects "Start process"
    Then user sees "Start process" page
    And user selects "Send administrative status" from the "Process" dropdown "1"
    When user clicks on "Start"
    Then user enters the following values
      | field                                                      | value |
      | Administrative status                                      | UIT   |
      | Estimated duration of com task execution in minutes        | 4     |
      | Max duration of check result subworkflow (format #d#h#m#s) | 2m    |
    And user clicks on "Start"
    When user sees "Processes" page
    Then user waits until there are no running "Send administrative status" process
    And user clicks "History" button of the Process Page
    Then user verifies if latest Process "Send administrative status" is completed and has the following results
      | variable | result    |
      | Status   | Completed |
    Examples:
      | DeviceNo          |
      | SIM_E000000019619 |
