@Run
Feature: Commission and Decommission a slave device

  Scenario Outline: 01 Commission a slave device
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<SlaveDeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<SlaveDeviceNo>"
    And user sees "Overview" page
    Then user clicks on the "Manage communication topology" link
    And user clicks on the "Edit master" icon
    Then user fills in "<MasterDeviceNo>" as the "Master"
    And user clicks on "Save"
    Then user verifies if "Master" has the value "<MasterDeviceNo>"
    Examples:
      | SlaveDeviceNo     | MasterDeviceNo    |
      | SIM_G000000019619 | SIM_E000000019619 |

  Scenario Outline: 02 Decommission a slave device
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<SlaveDeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<SlaveDeviceNo>"
    And user sees "Overview" page
    Then user clicks on the "Manage communication topology" link
    And user clicks on the "Remove master" icon
    Then user clicks on "Remove" button on the popup
    Then user verifies if "Master" has the value "-"
    Examples:
      | SlaveDeviceNo     |
      | SIM_G000000019619 |
