@Run
Feature: Create New Master Device Manually

  Scenario Outline:01 Create a new master device
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    When user goes to the "Devices" page of "MultiSense"
    Then user sees "Devices" page
    When user selects "Add device" under the "Device life cycle management" object
    Then user sees "Add device" page
    And user enters the following values
      | field         | value      |
      | Name          | <DeviceNo> |
      | Serial number | <DeviceNo> |
    And user selects "SAG E0048 3F LTE GS 5.0 SIM" from the "Device configuration" dropdown "1"
    And user selects "5:3_001" from the "Device configuration" dropdown "2"
    And user enters the following values
      | field         | value       |
      | Manufacturer  | Sagemcom    |
      | Model number  | 1           |
      | Model version | 1.01        |
      | Batch         | First batch |
    And user selects the date "02 January 2020"
    And user clicks on "Add"
    Then user verifies if "State" has the value "In creation (View history)"
    Examples:
      | DeviceNo          |
      | DUM_E100000000119 |

  Scenario Outline:02 Remove a master Device
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    When user clicks on the "Action" button and selects "Scrapped"
    Then user selects transition date radio button "2"
    And user selects the date "<Date>"
    And user clicks on "Next"
    And user clicks on "Finish"
    When user sees "Overview" page
    Then user verifies if "State" has the value "Scrapped (View history)"
    And user clicks on the "Action" button and selects "Remove"
    When user selects transition date radio button "1"
    And user clicks on "Next"
    Then user clicks on "Finish"
    And user sees "Devices" page
    When user searches for a device, with the "Name" "=" "<DeviceNo>"
    And user verifies that the device does not exists.
    Examples:
      | DeviceNo          |Date|
      | DUM_E100000000119 |07 January 2020|


