@Run
Feature: Start Stop Device Workflow

  Scenario Outline: 01 Start Stop Device Workflow
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<DeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<DeviceNo>"
    And user sees "Overview" page
    When user clicks on the "Action" button and selects "Start process"
    Then user sees "Start process" page
    And user selects "Stop Device" from the "Process" dropdown "1"
    When user clicks on "Start"
    And user sees "Processes" page
    #checks for the status of the process should be added
    Examples:
      | DeviceNo          |
      | SIM_E000000019619 |



