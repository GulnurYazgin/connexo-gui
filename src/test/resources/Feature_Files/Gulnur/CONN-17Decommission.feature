@Run
Feature: Decommission a slave device

  Scenario Outline: 01 Decommission a slave device via commands
    Given user logs in to "acc" environment
    When user goes to "MultiSense"
    Then user sees "Dashboard" page
    And user searches for a device, with the "Name" "=" "<SlaveDeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<SlaveDeviceNo>"
    And user sees "Overview" page
    Then user clicks on "Communication" from the sub menu
    And user clicks on "Commands" from the sub menu
    And user clicks on "Add command"
    Then user selects "MBus setup" from the "Command category" dropdown "1"
    Then user selects "Decommission" from the "Command" dropdown "1"
    And user saves the release date
    When user clicks on "Add"
    When user clicks on "Trigger"
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Commands G"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Commands G" has the status "Waiting"
    And user clicks on "Commands" from the sub menu
    Then user waits until the related command has the status "Confirmed"
    And user searches for a device, with the "Name" "=" "<MasterDeviceNo>"
    When user clicks "Search" button
    Then user clicks on search result "<MasterDeviceNo>"
    And user sees "Overview" page
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Topology E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Topology E" has the status "Waiting"
    And user clicks on "Overview" from the sub menu
    And user verifies that the slave/master device field is empty
    And user clicks on "Communication tasks" from the sub menu
    And user clicks on the action button of the communication task "Ad Hoc Events E"
    And user clicks on option "Run now"
    Then user waits until the communication task "Ad Hoc Events E" has the status "Waiting"
#    Then user clicks on "Data sources" from the sub menu
#    And user clicks on "Events" from the sub menu
#    #Make this step applicable only for ESMR meters
#    Then user verifies that event "47" is visible with the correct timestamp
    Examples:
      | SlaveDeviceNo     |MasterDeviceNo     |
      | G0072004011395220 |E0061000000062419  |