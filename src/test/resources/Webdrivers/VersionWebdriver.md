
# Version is updated on 30-04-2019 
Version **74.0.3729.6** is currently used.

# Release notes: 

###----------ChromeDriver v74.0.3729.6 (2019-03-14)----------
_Supports Chrome v74_
Resolved issue 2799: Recent change to WebDriver Tests causes ChromeDriver to fail W3C Tests 
Resolved issue 2798: GET /sessions is not working if at least one of the sessions is in W3C mode 
Resolved issue 2783: Improper UTF-8 encoding for CSS child selectors (driver.page_source)
Resolved issue 2782: ChromeDriver can't connect to Chrome when /dev/shm is not available on Linux 
Resolved issue 2773: Raise a standard exception when element is overlay by another element 
Resolved issue 2768: 2.46 produces unexpected debug.log file if verbose logging is enabled 
Resolved issue 2749: Update Switch To Frame error checks to match latest W3C spec 
Resolved issue  755: /session/:sessionId/doubleclick only generates one set of mousedown/mouseup/click events 
Resolved issue  427: Clicking div with SVG image causes "cannot read property 'length' of undefined" error. 

### ----------ChromeDriver 73.0.3683.68 (2019-03-06)----------
_Supports Chrome version 73_
* Resolved issue 2768: ChromeDriver produces unexpected debug.log file if verbose logging is enabled
* Resolved issue 2728: Is Element Displayed command does not work correctly with v0 shadow DOM inserts
* Resolved issue 2799: Recent change to WebDriver Tests causes ChromeDriver to fail W3C Tests
* Resolved issue 2744: Execute Script returns wrong error code when JavaScript returns a cyclic data structure
* Resolved issue 1529: OnResponse behavior can lead to port exhaustion 
* Resolved issue 2736: Close Window command should handle user prompts based on session capabilities
* Resolved issue 1963: Sending keys to disabled element should throw Element Not interactable error
* Resolved issue 2679: Timeout value handling is not spec compliant
* Resolved issue 2002: Add Cookie is not spec compliant
* Resolved issue 2716: Clearing Text Boxes
* Resolved issue 2714: ConnectException: Failed to connect to localhost/0:0:0:0:0:0:0:1:15756. Could not start driver.
* Resolved issue 2722: Execute Script does not correctly convert document.all into JSON format
* Resolved issue 2681: ChromeDriver doesn't differentiate "no such element" and "stale element reference"

### ----------ChromeDriver v2.46 (2019-02-05)----------
_Supports Chrome v71-73_
* Fixed error code returned from Execute Script command in some scenarios
* Made the HTTP server keep connection alive by default
* Fixed Close Window command to correctly handle user prompts
* Fixed error code returned while sending keys to disabled element
* Improved spec compliance of timeout value handling
* Improved spec compliance of Add Cookie command
* Improved spec compliance of Switch to Frame command
* Increased HTTP server listening queue length
* Fixed Is Element Displayed command in v0 shadow DOM
* Fixed Element Double Click command
* Added warning about Element Clear command behavior change in log file
* Fixed Execute Script command to correctly convert document.all into JSON format
* Improved handling of bad element reference

### ----------ChromeDriver v2.45 (2018-12-10)----------
_Supports Chrome v70-72_
* Resolved issue 1997: New Session is not spec compliant _[Pri-1]_
* Resolved issue 2685: Should Assert that the chrome version is compatible _[Pri-2]_
* Resolved issue 2677: Find Element command returns wrong error code when an invalid locator is used _[Pri-2]_
* Resolved issue 2676: Some ChromeDriver status codes are wrong _[Pri-2]_
* Resolved issue 2665: compile error in JS inside of WebViewImpl::DispatchTouchEventsForMouseEvents _[Pri-2]_
* Resolved issue 2658: Window size commands should handle user prompts _[Pri-2]_
* Resolved issue 2684: ChromeDriver doesn't start Chrome correctly with options.addArguments("user-data-dir=") _[Pri-3]_
* Resolved issue 2688: Status command is not spec compliant _[Pri-3]_
* Resolved issue 2654: Add support for strictFileInteractability _[Pri-]_

### ----------ChromeDriver v2.44 (2018-11-19)---------- 

_Supports Chrome v69-71_
* Resolved issue 2522: Test ChromeDriverTest.testWindowMaximize is failing on Mac build bot on Waterfall _[Pri-2]_
* Resolved issue 2615: Incorrect 'alert open error' for window handle call _[Pri-2]_
* Resolved issue 2649: Element Send Keys should get "text" property in W3C mode _[Pri-2]_
* Resolved issue 1995: XML special case of Is Element Enabled is not handled as per spec _[Pri-2]_
* Resolved issue 1994: XML special case of Get Element CSS Value is not handled as per spec _[Pri-2]_
* Resolved issue 2655: Set Window Rect needs to check for invalid input _[Pri-3]_
* Resolved issue 2597: Support new unhandledPromptBehavior modes _[Pri-3]_

### ----------ChromeDriver v2.43 (2018-10-16)----------

_Supports Chrome v69-71_
* Resolved issue 2537: Parsing of proxy configuration is not standard compliant _[Pri-1]_
* Resolved issue 2607: Launch App command is flaky _[Pri-2]_
* Resolved issue 2575: Screenshot of element inside iFrame is taken incorrectly _[Pri-2]_
* Resolved issue 1855: Feature request : ChromeDriver to support window resizing over a remote connection _[Pri-2]_
* Resolved issue 1998: Error codes are not handled in Clear element _[Pri-2]_
* Resolved issue 2016: Not Waiting until element is visible _[Pri-2]_
* Resolved issue 1936: Get element property is not implemented _[Pri-2]_
* Resolved issue 1992: Switch To Frame is not spec compliant _[Pri-2]_
* Resolved issue 2001: Execute Async Script does not return spec compliant error codes _[Pri-2]_
* Resolved issue 2000: Execute Script does not return spec compliant error codes _[Pri-2]_
* Resolved issue 1987: Error code in ExecuteGet is not conformant with spec _[Pri-2]_
* Resolved issue 2003: Send Alert Text is not returning spec compliant error codes _[Pri-2]_
* Resolved issue 1319: clear() on an input type="date" pretends element is not user-editable _[Pri-3]_
* Resolved issue 1404: Chromedriver gets window handle for the tab which is opened manually _[Pri-3]_
* Resolved issue  586: Allow append or start a new log file for chromedriver _[Pri-3]_
* Resolved issue 2371: New Session does not invoke w3c mode if flag is in firstMatch _[Pri-]_