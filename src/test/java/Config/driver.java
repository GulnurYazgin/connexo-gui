package Config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class driver {


    public static WebDriver instance;

    public static Integer timeOutInSec=100;

    public static void setWait(){
        instance.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
    }

    public static void maximize(){
        instance.manage().window().maximize();
    }

    public static void goToUrl(String url){
        instance.navigate().to(url);
    }

    public static void setDriverLocation(String os) {
        if (os.contains("Portable")) {

            System.setProperty("webdriver.chrome.driver", "src/test/resources/Webdrivers/chromedriverNew.exe");

            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("start-maximized"); // open Browser in maximized mode
            chromeOptions.addArguments("disable-infobars"); // disabling infobars
            chromeOptions.addArguments("--disable-extensions"); // disabling extensions
            chromeOptions.addArguments("--disable-gpu"); // applicable to windows os only
            chromeOptions.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
            chromeOptions.addArguments("--no-sandbox"); // Bypass OS security model
            chromeOptions.addArguments("--headless");
            chromeOptions.setBinary("src/test/resources/Webdrivers/GoogleChromePortableDev/GoogleChromePortable.exe");
            instance = new ChromeDriver(chromeOptions);
            instance = new ChromeDriver();

        }

        else if (os.contains("Firefox")) {

            System.setProperty("webdriver.gecko.driver","src/test/resources/Webdrivers/geckodriver.exe");

            File pathBinary = new File("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
            FirefoxBinary firefoxBinary = new FirefoxBinary(pathBinary);
            DesiredCapabilities desired = DesiredCapabilities.firefox();
            FirefoxOptions options = new FirefoxOptions();
            desired.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options.setBinary(firefoxBinary));
            instance = new FirefoxDriver(options);

        }
        else if (os.contains("OpenLaptop")){
            System.setProperty("webdriver.chrome.driver", "src/test/resources/Webdrivers/chromedriverOld.exe");

            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("start-maximized"); // open Browser in maximized mode
            chromeOptions.addArguments("disable-infobars"); // disabling infobars
            chromeOptions.addArguments("--disable-extensions"); // disabling extensions
            chromeOptions.addArguments("--disable-gpu"); // applicable to windows os only
            chromeOptions.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
            chromeOptions.addArguments("--no-sandbox"); // Bypass OS security model
            //chromeOptions.addArguments("--headless");
            chromeOptions.setBinary(new File("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"));
            instance = new ChromeDriver(chromeOptions);

        }

        else if (os.contains("Chrome")){
            System.setProperty("webdriver.chrome.driver", "src/test/resources/Webdrivers/chromedriverNew.exe");

            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("start-maximized"); // open Browser in maximized mode
            chromeOptions.addArguments("disable-infobars"); // disabling infobars
            chromeOptions.addArguments("--disable-extensions"); // disabling extensions
            chromeOptions.addArguments("--disable-gpu"); // applicable to windows os only
            chromeOptions.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
            chromeOptions.addArguments("--no-sandbox"); // Bypass OS security model
            chromeOptions.addArguments("--headless");
            instance = new ChromeDriver(chromeOptions);

        }
    }

}
