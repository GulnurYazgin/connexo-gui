package stepDefs;

import Pages.*;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static Config.driver.instance;
import static Pages.GenericPage.*;
import static Pages.addDevicePage.*;
import static stepDefs.GenericFunctions.waitAndClick;
import static stepDefs.GenericFunctions.waitAndTypeInTextField;

public class createMasterDevice {

    @When("^user goes to the \"([^\"]*)\" page of \"([^\"]*)\"$")
    public void user_goes_to_the_page_of(String pageMenu, String mainSubMenu) {
        waitAndClick(mainMenuButton());
        waitAndClick(mainSubMenu(mainSubMenu));
        waitAndClick(pageMenu(pageMenu));
    }

    @When("^user selects \"([^\"]*)\" under the \"([^\"]*)\" object$")
    public void user_selects_under_the_object(String subSection, String sectionTitle) {
        waitAndClick(pageSection(sectionTitle,subSection));
    }

    @Then("^user enters the following values$")
    public void user_enters_the_following_values(DataTable content) {
        List<Map<String, String>> list = content.asMaps(String.class, String.class);
        for (int i = 0; i < list.size(); ) {
            waitAndTypeInTextField(textField(list.get(i).get("field")),list.get(i).get("value"));
            i++;
        }
    }

    @Then("^user selects \"([^\"]*)\" from the dropdown \"([^\"]*)\"$")
    public void user_selects_from_the_dropdown(String content, String element) {
        waitAndTypeInTextField(deviceConfigurationField(element),content);
    }

    @Then("^user selects \"([^\"]*)\" from the \"([^\"]*)\" dropdown \"([^\"]*)\"$")
    public void user_selects_from_the_dropdown(String content, String element, String dropdownNumber) throws Exception {
        Thread.sleep(3000);
        waitAndClick(dropdown(element,dropdownNumber));
        Thread.sleep(3000);
        waitAndClick(addDeviceDropdownSelection(content));
    }

    @Then("^user selects the date \"([^\"]*)\"$")
    public void user_selects_the_date(String dateString) {
        String[] dateStr = dateString.split(" ");
        String day=dateStr[0];
        String month=dateStr[1];
        String year=dateStr[2];
        String monthSubstring = month.substring(0, 3);
        String date = month + " " + day + ", " + year;
        waitAndClick(addDevicePage.dateFieldButton2());
        waitAndClick(addDevicePage.dateButton());
        waitAndClick(addDevicePage.dateMonth(monthSubstring));
        waitAndClick(addDevicePage.dateYear(year));
        waitAndClick(addDevicePage.dateOKButton2());
        waitAndClick(addDevicePage.visibleDay(date));
    }

    @Then("^user clicks on \"([^\"]*)\"$")
    public void user_clicks_on(String button) throws Exception {
        Thread.sleep(2000);
        try {
            if(button.equals("Add command")) {
                try {
                    waitAndClick(commandsPage.buttonCommands());
                }
                catch(TimeoutException e) {  //NoSuchElementException is not needed here.
                    waitAndClick(addDevicePage.button(button));
                }
            }
            else{
                Thread.sleep(2000);
                    waitAndClick(addDevicePage.button(button));
            }
            Thread.sleep(5000);
        } catch (NoSuchElementException e) {
            System.out.println("No "+button+" button found");
        }
    }

    @Then("^user verifies if \"([^\"]*)\" has the value \"([^\"]*)\"$")
public void user_verifies_if_has_the_value(String field, String check) throws Exception {
        Thread.sleep(5000);
        switch (field) {
            case "Firmware version":
                GenericFunctions.getValueAndCompare(check, firmwarePage.firmwareVersionFieldPerFirmware(field));
                break;
            case "":
                GenericFunctions.getValueAndCompare(check, deviceSummaryField(field));
                break;
        }
    }

    @When("^user goes to \"([^\"]*)\"$")
    public void user_goes_to(String subMenu) throws Exception {
        Thread.sleep(7000);
        waitAndClick(mainMenuButton());
        waitAndClick(mainSubMenu(subMenu));
    }

    @When("^user searches for a device, with the \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void user_fill_in_Device_search_information_with(String searchCriteria,String dropdownSelection ,String searcehedDevice) throws Exception {
        waitAndClick(addDevicePage.searchButton());
        waitAndClick(addDevicePage.searchCriteria(searchCriteria));
        waitAndClick(addDevicePage.dropdownSearchCriteria());
        waitAndClick(addDevicePage.searchCriteriaDropdownSelection(dropdownSelection));
        waitAndTypeInTextField(addDevicePage.searchCriteriaTextField(),searcehedDevice);
    }

    @When("^user clicks \"([^\"]*)\" button$")
    public void user_clicks_button(String button) {
        waitAndClick(addDevicePage.searchCriteriaButton(button));
    }

    @Then("^user clicks on search result \"([^\"]*)\"$")
    public void user_clicks_on_search_result(String searchResult) {
        waitAndClick(addDevicePage.searchResult(searchResult));
    }

    @Then("^user clicks on the \"([^\"]*)\" button and selects \"([^\"]*)\"$")
    public void user_clicks_on_the_button_and_selects(String firstButton, String secondButton) {
        instance.navigate().refresh();
        instance.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        waitAndClick(GenericPage.button(firstButton));
        waitAndClick(GenericPage.buttonSelection(secondButton));
    }

    @Then("^user selects transition date radio button \"([^\"]*)\"")
    public void user_selects_transition_date_radio_button_and_clicks(String buttonNumber) {
        waitAndClick(addDevicePage.radioButton(buttonNumber));
    }
    @Then("^user verifies that the device does not exists\\.$")
    public void user_verifies_that_the_device_does_not_exists() {
        GenericFunctions.getValueAndCompare("No search results found",noSearchResultsFound());
    }

    @Then("^user checks \"([^\"]*)\" checkbox on$")
    public void user_checks_checkbox_on(String checkboxName) {
        waitAndClick(textField(checkboxName));
    }
}
