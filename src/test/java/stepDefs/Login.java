package stepDefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.NoSuchElementException;

import static Config.Config_Connexo.*;
import static Config.driver.goToUrl;
import static Pages.GenericPage.headerPageText;
import static Pages.loginPage.*;
import static stepDefs.GenericFunctions.getValueAndCompare;
import static stepDefs.GenericFunctions.waitForElementToBeVisible;


public class Login {


    @Given("^user logs in to \"([^\"]*)\" environment$")
    public void user_logs_in(String environment) throws Exception {
        String URL = "https://" + getEnvironmentAgain(environment);// + "/apps/login/index.html"
        goToUrl(URL);
            Thread.sleep(5000);
        GenericFunctions.waitAndClick(SsoUserName());
        GenericFunctions.waitAndTypeInTextField(SsoUserName(), getSsoUsername());
        GenericFunctions.waitAndClick(SsoLoginSubmitButton());
        Thread.sleep(5000);
        try{
        GenericFunctions.waitAndClick(SsoPassword());
        GenericFunctions.waitAndTypeInTextField(SsoPassword(), getSsoPassword());
        GenericFunctions.waitAndClick(aanmeldenButton());}
        catch(NoSuchElementException exception){
            System.out.println("Automatically logged in");
        }
        Thread.sleep(15000);
        //try catch here.
        try {
            GenericFunctions.waitAndClick(userName());
            GenericFunctions.waitAndTypeInTextField(userName(), getUsername());
            GenericFunctions.waitAndClick(password());
            GenericFunctions.waitAndTypeInTextField(password(), getPassword());
            GenericFunctions.waitAndClick(loginSubmitButton());
        }
        catch (NoSuchElementException exception){
            System.out.println("Automatically logged in");
        }
    }

    @Then("^user sees \"([^\"]*)\" page$")
    public void user_sees_page(String header) throws Exception {
        Thread.sleep(10000);
        waitForElementToBeVisible(headerPageText(header));
        getValueAndCompare(header,headerPageText(header));

    }

}
