package stepDefs;

import Pages.*;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static Config.driver.instance;
import static Pages.GenericPage.*;
import static Pages.addDevicePage.addDeviceDropdownSelection;
import static Pages.addDevicePage.dropdown;
import static Pages.adminPage.processTableItem;
import static Pages.startProcessPage.*;
import static org.junit.Assert.assertNotEquals;
import static stepDefs.GenericFunctions.*;

public class usableSteps {
    public String processID;
    public String updateTime;
    public String administrativeStatus;
    public String data;


    public String getProcessID() {

        return processID;
    }
    public String getAdministrativeStatus() {

        return administrativeStatus;
    }

    public String getUpdateTime() {

        return updateTime;
    }
    public String getData() {

        return data;
    }

    public String setProcessID(String processNR) {
        this.processID = processNR;
        return processID;
    }
    public String setAdministrativeStatus(String administrativeStatus) {
        this.administrativeStatus = administrativeStatus;
        return administrativeStatus;
    }

    public String setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
        return updateTime;
    }
    public String setData(String data) {
        this.data = data;
        return data;
    }


    @Then("^user verifies if latest Process \"([^\"]*)\" is completed and has the following results$")
    public void user_verifies_if_latest_Process_is_completed_and_has_the_following_results(String processName, DataTable content) throws Exception {
        //set&get process id....
        Thread.sleep(10000);
        String savedProcessID = getProcessID();
        System.out.println("The process id is" + savedProcessID);
        List<Map<String, String>> list = content.asMaps(String.class, String.class);
        for (int i = 0; i < list.size(); ) {
            System.out.println("Get list per item" + list.get(i));
            if (list.get(i).get("variable").equals("Status")) {
                GenericFunctions.getValueAndCompare(list.get(i).get("result"), historyPageStatus(getProcessID()));//burda da get process id olacak
                break;
            }
        }
    }

    @Then("^user waits until there are no running \"([^\"]*)\" process$")
    public void user_waits_until_there_are_no_running_processes(String processName) throws Exception {
        setProcessID(processNumber(processName).getText());
        System.out.println("Process ID is:" + getProcessID());
        boolean check = false;
        while (!check) {
            try {
                if (instance.findElement(By.xpath("//td[contains(.,'" + getProcessID().trim() + "')]/div")).isDisplayed()) {// //historyPageProcessID(getProcessID()).isDisplayed()
                    System.out.println("process id is visible on the page");
                    instance.navigate().refresh();
                    Thread.sleep(20000);
                }
            } catch (Exception e) {
                System.out.println("process id not visible, process is completed");
                check = true;
            }
        }
    }

    @Then("^user waits until there are no running \"([^\"]*)\" process with the id \"([^\"]*)\"$")
    public void user_waits_until_there_are_no_running_processes_with_id(String processName, String id) throws Exception {
        setProcessID(id);
        System.out.println("Process ID is:" + getProcessID());
        waitAndClick(processTab("History"));
        boolean check = false;
        while (!check) {
            if (instance.findElement(By.xpath("//td[contains(.,'" + getProcessID().trim() + "')]/div")).isDisplayed()) {// //historyPageProcessID(getProcessID()).isDisplayed()
                System.out.println("process id is visible on the page");
                waitAndClick(specificProcessID(getProcessID()));
                check = true;
            } else {
                System.out.println("process id not visible, refreshing");
                System.out.println("the xpath is" + historyPageProcessID(getProcessID()));
                waitAndClick(historyPageApplyButton());
                Thread.sleep(20000);
            }
        }
    }

    @Then("^user clicks \"([^\"]*)\" button of the Process Page$")
    public void user_clicks_button_of_the_Process_Page(String processTabName) throws Exception {
        waitAndClick(processTab(processTabName));
    }

    @When("^user clicks on \"([^\"]*)\" from the sub menu$")
    public void user_clicks_on_x_from_the_sub_menu(String selection) throws InterruptedException {
        instance.navigate().refresh();
        Thread.sleep(15000);
        waitAndClick(devicePage.subMenuOptions(selection));
    }

    @When("^user goes to the device \"([^\"]*)\"$")
    public void user_goes_to_the_device(String device) throws Exception {
        Thread.sleep(15000);
        waitAndClick(mainMenuButton());
        waitAndClick(mainSubMenu("MultiSense"));
        Thread.sleep(15000);
        waitForElementToBeVisible(headerPageText("Dashboard"));
        getValueAndCompare("Dashboard", headerPageText("Dashboard"));
        Thread.sleep(10000);
        waitAndClick(addDevicePage.searchButton());
        waitAndClick(addDevicePage.searchCriteria("Name"));
        waitAndClick(addDevicePage.dropdownSearchCriteria());
        waitAndClick(addDevicePage.searchCriteriaDropdownSelection("="));
        waitAndTypeInTextField(addDevicePage.searchCriteriaTextField(), device);
        waitAndClick(addDevicePage.searchCriteriaButton("Search"));
        Thread.sleep(10000);
        waitAndClick(addDevicePage.searchResult(device));
        Thread.sleep(10000);
        waitForElementToBeVisible(headerPageText("Overview"));
        getValueAndCompare("Overview", headerPageText("Overview"));
    }

    @Then("^user clicks on the \"([^\"]*)\" link$")
    public void user_clicks_on_the_link(String linkName) {
        waitAndClick(devicePage.overviewLink(linkName));
    }

    @Then("^user fills in \"([^\"]*)\" as the \"([^\"]*)\"$")
    public void user_fills_data_in_text_field(String content, String element) {
        waitAndTypeInTextField(textField(element), content);
    }

    @And("^user clicks on the \"([^\"]*)\" icon$")
    public void user_clicks_on_the_special_icon(String buttonIcon) {
        waitAndClick(devicePage.iconButton(buttonIcon));
    }

    @Then("^user clicks on \"([^\"]*)\" button on the popup$")
    public void userClicksOnButtonOnThePopup(String selection) {
        waitAndClick(GenericPage.popupButton(selection));
    }

    @And("^user sees the popup message \"([^\"]*)\"$")
    public void userSeesThePopupMessage(String message) throws InterruptedException {
        Thread.sleep(2000);
        GenericFunctions.waitForElementToBeVisible(popupMessage(message));
    }

//    @Then("^user clicks on side-menu item \"([^\"]*)\"$")
    @And("^user clicks on option \"([^\"]*)\"$")
    public void userClicksOnSideMenuItem(String item) throws InterruptedException {
        Thread.sleep(10000);
        waitAndClick(GenericPage.sideMenuItem(item));
    }

    @And("^user clicks on the action button of the communication task \"([^\"]*)\"$")
    public void userSelectsTheCommunicationTask(String communicationTaskName) throws InterruptedException {
        Thread.sleep(10000);
        waitAndClick(GenericPage.communicationTaskName(communicationTaskName));
    }

    @Then("^user waits until the communication task \"([^\"]*)\" has the status \"([^\"]*)\"$")
    public void userWaitsUntilTheCommunicationTasHasTheStatus(String communicationTaskName, String expectedResult) throws InterruptedException {
        //status waiting olana kadar refresh. buraya extradan bir failed eklemek lazim ve never completed mi ne ondan
        instance.navigate().refresh();
        Thread.sleep(7000);
        while (!GenericPage.communicationTaskStatus(communicationTaskName).getText().equals(expectedResult)) {
            System.out.println("Found status is " + GenericPage.communicationTaskStatus(communicationTaskName).getText());
            if (GenericPage.communicationTaskStatus(communicationTaskName).getText().equals("Failed") || GenericPage.communicationTaskStatus(communicationTaskName).getText().equals("Never completed")) {
                System.out.println("The communication task was not successful");
                break;
            } else {
                instance.navigate().refresh();
                Thread.sleep(20000);
            }
        }
    }

    @Then("^user clicks on the action button of the firmware type \"([^\"]*)\"$")
    public void userClicksOnTheActionButtonOfThe(String firmwareType) throws InterruptedException {
        Thread.sleep(10000);
        waitAndClick(firmwarePage.firmwareActionButton(firmwareType));
    }

    @Then("^user selects \"([^\"]*)\" from the \"([^\"]*)\" dropdown$")
    public void userSelectsFromTheDropdown(String selection, String dropdownName) throws InterruptedException {
        instance.navigate().refresh();
        Thread.sleep(5000);
        waitAndClick(itemsPerPageDropdown(dropdownName));
        waitAndClick(addDevicePage.searchCriteriaDropdownSelection(selection));
        Thread.sleep(5000);
    }

    @And("^user verifies if register \"([^\"]*)\" has the value \"([^\"]*)\"$")
    public void userVerifiesIfRegisterHasTheValue(String registerName, String registerValue) {
        GenericFunctions.getValueAndCompare(registerValue, registerValue(registerName));
    }

    @Then("^user clicks on the dropdown number \"([^\"]*)\" and selects \"([^\"]*)\"$")
    public void userClicksOnTheDropdownNumberAndSelects(String dropdownNo, String selection) throws Throwable {
        instance.navigate().refresh();
        Thread.sleep(5000);
        waitAndClick(firmwarePage.dropdownByNumber(dropdownNo));
        Thread.sleep(2000);
        if ("Firmware".equals(selection)) {
            waitAndClick(firmwarePage.firmwarePageDropdownSelection(selection));
        } else if ("LTE Diagnostics and Statistics".equals(selection)) {
            waitAndClick(registersPage.registerListItem(selection));
        }
    }

    @Then("^user saves the starting time of the firmware upgrade$")
    public void userSavesTheStartingTimeOfTheFirmwareUpgrade() throws ParseException {
        //history tabina bas
        waitAndClick(processTab("History"));
        //time i get yap.
        setUpdateTime(firmwarePage.firmwareUpgradeTime().getText());
        System.out.println("The firmware upgrade time is" + getUpdateTime());
        instance.navigate().refresh();
//        //Upload and activation of version/image 04.11 pending (Upload planned on Sep-02-'20 at 11:51 am, activation planned on Sep-02-'20 at 11:51 am)
//        String dateFirmwareUpgrade=completeFirmwareUpgradeText.substring(72, 82);
//        String timeFirmwareUpgrade=completeFirmwareUpgradeText.substring(86, 92);
//        System.out.println("The date is " +dateFirmwareUpgrade+" and the time is "+ timeFirmwareUpgrade);
//        setFirmwareUpgradeTime(GenericFunctions.changeDateTimeFormat(dateFirmwareUpgrade,timeFirmwareUpgrade,"timeFWU"));
//        System.out.println("the firmware upgrade time in the get is" +getFirmwareUpgradeTime());
//        setComTaskTime(GenericFunctions.changeDateTimeFormat(dateFirmwareUpgrade,timeFirmwareUpgrade,"timeComTask"));
//        System.out.println("the comTaskTime in the get is" +getComTaskTime());
        //The date is 2 Sep '2 and the time is 12:58,
        //Aug-11-'20 at 08:42 am
    }

    @Then("^user verifies that event \"([^\"]*)\" is visible with the correct timestamp$")
    public void userVerifiesThatEventIsVisibleWithTheCorrectTimestamp(String eventNo) throws InterruptedException {
            instance.navigate().refresh();
            Thread.sleep(7000);
            while (!timeOfUsePage.commandsStatus(getUpdateTime()).getText().equals(eventNo)) {
                instance.navigate().refresh();
                Thread.sleep(20000);
            }
        }

    @And("^user waits until the firmware update is completed$")
    public void userWaitsUntilTheFirmwareUpdateIsCompleted() throws InterruptedException {
        boolean check = false;
        while (!check) {
            if (firmwarePage.firmwareUpgradeStatus(getUpdateTime()).getText().equals("Pending")) {
                System.out.println("Firmware update process is still ongoing, refreshing");
                instance.navigate().refresh();
                Thread.sleep(20000);
            } else {
                check = true;
            }
        }
    }

    @Then("^user verifies if connection method \"([^\"]*)\" has the last connection \"([^\"]*)\" and the status \"([^\"]*)\"$")
    public void stuser_verifies_if_process_name_has_the_status(String processName, String valueLastConnection, String valueStatus) throws Exception {
        getValueAndCompare(valueLastConnection, processTableItem(processName, "3"));
        getValueAndCompare(valueStatus, processTableItem(processName, "5"));
    }

    @And("^user saves the \"([^\"]*)\" time$")
    public void userSavesTheTime(String valueName) {
        setUpdateTime(timeOfUsePage.deviceSettingText(valueName).getText().substring(5, 24));
        System.out.println("The release time of the send calendar is" + getUpdateTime());
    }

    @And("^user selects radio button number \"([^\"]*)\" from the \"([^\"]*)\" field$")
    public void userSelectsRadioButtonNumberFromTheField(String radioButtonNo, String radioButtonFieldName) throws InterruptedException {
        Thread.sleep(5000);
        waitAndClick(timeOfUsePage.radioFieldSelection(radioButtonFieldName, radioButtonNo));
    }

    @And("^user selects the timestamp which is -1 hour from the actual time$")
    public void userSelectsTheTimestampWhichIsHourFromTheActualTime() {
        waitAndClick(timeOfUsePage.firstButtonDown());
    }

    @Then("^user selects \"([^\"]*)\" from the command action button$")
    public void userSelectsFromTheCommandActionButton(String selection) throws InterruptedException {
        Thread.sleep(2000);
        waitAndClick(timeOfUsePage.commandsActionButton(getUpdateTime()));
        waitAndClick(buttonSelection(selection));
    }

    @Then("^user waits until the related command has the status \"([^\"]*)\"$")
    public void userWaitsUntilTheCommandHasTheStatus(String expectedResult) throws InterruptedException {
        instance.navigate().refresh();
        Thread.sleep(7000);
        while (!timeOfUsePage.commandsStatus(getUpdateTime()).getText().equals(expectedResult)) {
            instance.navigate().refresh();
            Thread.sleep(20000);
        }
    }

    @Then("^user verifies that the \"([^\"]*)\" is \"([^\"]*)\"$")
    public void userVerifiesThatTheIs(String settingName, String expectedResult) throws Throwable {
        Thread.sleep(5000);
        getValueAndCompare(expectedResult, timeOfUsePage.calendarSetting(settingName));
    }

    @Then("^user checks the \"([^\"]*)\" register and prints RSRP value in dB$")
    public void userChecksTheRegisterAndPrintsRSRPValueInDB(String registerName) {
        String registerValue = registersPage.registerValue(registerName).getText();
//            System.out.println("the value of the register is" +registerValue);
        String[] LTEmonitoringDataArray = registerValue.split("; ");
        String RSRP = LTEmonitoringDataArray[3].substring(5, 7);
        Integer RSRPintFinal = (140 - Integer.valueOf(RSRP));
        if (50 <= RSRPintFinal && RSRPintFinal <= 120) {
            System.out.println("Signal Strength(RSRP) of the meter is good and the value is: " + RSRPintFinal + "dB");
        } else {
            System.out.println("Signal Strength(RSRP) of the meter is bad and the value is: " + RSRPintFinal + "dB");
        }
    }

    @Then("^user checks the \"([^\"]*)\" register and prints the value$")
    public void userChecksTheRegisterAndPrintsTheValue(String registerName){
        setData(registersPage.registerValue(registerName).getText());
        System.out.println("the value of the register "+registerName+" is " +getData());
    }

    @Then("^user selects the new administrative status from the \"([^\"]*)\" dropdown \"([^\"]*)\"$")
    public void userSelectsTheNewAdministrativeStatusFromTheDropdown(String element, String dropdownNumber) throws Throwable {
        Thread.sleep(5000);
        System.out.println("the present administrative status on the meter is "+getData());
        waitAndClick(dropdown(element,dropdownNumber));
        if(getData().equals("1")||getData().equals("null")) {
            System.out.println("the present administrative status on the meter is "+getData()+" so the 2 will be selected");
            waitAndClick(addDeviceDropdownSelection("2"));
            setData("2");
        }
        else{
            System.out.println("the present administrative status on the meter is "+getData()+" so the 1 will be selected");
            waitAndClick(addDeviceDropdownSelection("1"));
            setData("1");
        }
    }

    @And("^user saves the release date$")
    public void userSavesTheReleaseDate() {
        String dateField=commandsPage.timeField("1").getAttribute("value");
        String hourField=commandsPage.timeField("2").getAttribute("value");
        String minuteField=commandsPage.timeField("3").getAttribute("value");
        setUpdateTime(GenericFunctions.buildTimeString(dateField,hourField,minuteField));
        System.out.println("The time which will be used in xpath is:" +getUpdateTime());
    }

    @Then("^user verifies if register \"([^\"]*)\" has the correct value$")
    public void userVerifiesIfRegisterHasTheCorrectValue(String registerName){
        getValueAndCompare(getData(), registersPage.registerValue(registerName));
    }

    @And("^user verifies that the slave/master device has the value \"([^\"]*)\"$")
    public void userVerifiesThatTheSlaveDeviceIsVisibleAndHasTheValue(String expectedValue) throws InterruptedException {
        Thread.sleep(5000);
        getValueAndCompare(expectedValue, devicePage.slaveDeviceField(expectedValue));
    }

    @Then("^user clicks on the slave/master device$")
    public void userClicksOnTheSlaveMasterDevice() throws InterruptedException {
        Thread.sleep(3000);
        waitAndClick(devicePage.MasterOrSlaveDeviceField());
    }

    @And("^user verifies that the slave/master device field is empty$")
    public void userVerifiesThatTheSlaveMasterDeviceFieldIsEmpty() throws InterruptedException {
        Thread.sleep(5000);
        getValueAndCompare("-", devicePage.emptyMasterOrSlaveDeviceField());
    }

    @And("^user clicks on the action button of the \"([^\"]*)\"$")
    public void userClicksOnTheSecurityAccessor(String item) throws InterruptedException {
        Thread.sleep(10000);
        waitAndClick(securityAccessorsPage.securityAccessorActionButton(item));
    }

    @Then("^user reads and prints the \"([^\"]*)\" value$")
    public void userReadsAndPrintsTheValue(String fieldName) {
        setData(addDevicePage.deviceSummaryField(fieldName).getText());
        System.out.println("The value of the field " +fieldName+ " is :"+getData());
    }

    @And("^user confirms that the actual firmware version of the meter is not the same as \"([^\"]*)\"$")
    public void userConfirmsThatTheActualFirmwareVersionOfTheMeterIsNotTheSameAs(String firmwareVersion){
        System.out.println("The value of the current firmware is " +getData());
        System.out.println("The value of the new firmware is " +firmwareVersion);
        assertNotEquals(firmwareVersion,getData());
    }
}
