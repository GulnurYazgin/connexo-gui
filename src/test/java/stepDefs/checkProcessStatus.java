package stepDefs;

import cucumber.api.java.en.Then;

import static Pages.adminPage.processTableItem;
import static stepDefs.GenericFunctions.getValueAndCompare;

public class checkProcessStatus {
    @Then("^user verifies if process name \"([^\"]*)\" has the status \"([^\"]*)\"$")
    public void user_verifies_if_process_name_has_the_status(String processName, String value) throws Exception {
        getValueAndCompare(value, processTableItem(processName,"4"));
    }
}
