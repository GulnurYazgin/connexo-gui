package stepDefs;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static Config.driver.instance;
import static Config.driver.timeOutInSec;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GenericFunctions {


    public static void waitAndClick(WebElement element) {
        WebDriverWait check = new WebDriverWait(instance, timeOutInSec);
        check.until(ExpectedConditions.visibilityOf(element));
        Actions action = new Actions(instance);
        action.moveToElement(element).click().build().perform();
    }
    public static void waitAndClickNoVisibility(WebElement element) {
        WebDriverWait check = new WebDriverWait(instance, timeOutInSec);
        Actions action = new Actions(instance);
        action.moveToElement(element).click().build().perform();
    }

    public static void waitForElementToBeVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(instance, timeOutInSec);
        wait.until(ExpectedConditions.visibilityOf(element));
        element.isDisplayed();
    }

    public static void waitAndTypeInTextField(WebElement element, String content) {
        waitForElementToBeVisible(element);
        element.clear();
        element.sendKeys(content);
        element.sendKeys(Keys.TAB);
    }

    public static void getValueAndCompare(String value, WebElement element) {
        String actual = element.getText();
        System.out.println("actual : " + actual);
        assertEquals(actual, value);
        System.out.println("expected : " + value);
    }
    public static String buildTimeString(String date, String hour, String minute) {
        String dateString3 = hour+":"+minute;
        //old format
//        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
//        try{
//            Date date3 = sdf.parse(dateString3);
//            //new format
//            SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm");
//            //formatting the given time to new format with AM/PM
//            String newFormatDateAndTime=date+" at "+sdf2.format(date3);
//            return newFormatDateAndTime;
//        }catch(ParseException e){
//            e.printStackTrace();
//        }
//        return null;

            String newFormatDateAndTime=date+" at "+dateString3;
            System.out.println("the returned date is "+newFormatDateAndTime);
            return newFormatDateAndTime;
    }
}



