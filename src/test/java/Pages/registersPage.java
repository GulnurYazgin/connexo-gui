package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class registersPage {
    public static WebElement registerValue(String registerName) {
        return instance.findElement(By.xpath(".//tr[contains(.,'"+registerName+"')]/td[3]"));
    }
    public static WebElement registerListItem(String itemName) {
        return instance.findElement(By.xpath("//li[contains(.,'"+itemName+"')]"));
    }
}
