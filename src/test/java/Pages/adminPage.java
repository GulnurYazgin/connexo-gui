package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class adminPage {
    public static WebElement processTableItem(String rowName, String columnNumber) {
        return instance.findElement(By.xpath(".//tr[contains(.,\""+rowName+"\")]/td["+columnNumber+"]"));
    }
}