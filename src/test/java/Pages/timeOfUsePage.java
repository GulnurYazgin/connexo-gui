package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class timeOfUsePage {
    public static WebElement deviceSettingText(String fieldName) {
        return instance.findElement(By.xpath(".//td[contains(.,'"+fieldName+"')]/following-sibling::td//div"));
    }
    public static WebElement radioFieldSelection(String fieldName,String fieldNo) {
        return instance.findElement(By.xpath("(//td[label[text()='"+fieldName+"']]/following-sibling::td[1]//td//input)["+fieldNo+"]"));
    }
    public static WebElement firstButtonDown() {//ilerde bunu degisebilirik xpath 1 2 3 olarak gerekirse
        return instance.findElement(By.xpath("(//div[contains(@class,'spinner-down')])[1]"));
    }
    public static WebElement commandsActionButton(String timeOfCommand) {
        return instance.findElement(By.xpath(".//tr[contains(.,\""+timeOfCommand+"\")]//span[contains(@class,'action')]"));
    }
    public static WebElement commandsStatus(String timeOfCommand) {
        return instance.findElement(By.xpath(".//tr[contains(.,\""+timeOfCommand+"\")]//td[3]"));
    }
    public static WebElement calendarSetting(String settingName) {
        return instance.findElement(By.xpath("(.//td[contains(.,'"+settingName+"')]/following-sibling::td//div)[1]"));
    }

}
