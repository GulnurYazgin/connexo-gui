package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.*;

public class loginPage {

    public static WebElement userName() {
        return instance.findElement(By.xpath(".//input[@name='username']"));
    }
    public static WebElement SsoUserName() {
        return instance.findElement(By.xpath(".//input[@name='loginfmt']"));
    }
    public static WebElement SsoPassword() {
        return instance.findElement(By.xpath(".//input[@name='Password']"));
    }
    public static WebElement password() {
        return instance.findElement(By.xpath(".//input[@name='password']"));
    }

    public static WebElement loginSubmitButton() {
        return instance.findElement(By.id("button-1017-btnIconEl"));
    }
    public static WebElement SsoLoginSubmitButton() {
        return instance.findElement(By.xpath(".//input[@type='submit']"));
    }
    public static WebElement aanmeldenButton() {
        return instance.findElement(By.xpath(".//span[@id='submitButton']"));
    }


}




