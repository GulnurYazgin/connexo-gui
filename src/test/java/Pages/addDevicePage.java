package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class addDevicePage {
    public static WebElement dateButton() {
        return instance.findElement(By.xpath("//span[starts-with(@id,'splitbutton')][@class='x-btn-button']"));
    }
    public static WebElement visibleDay(String visibleDay) {
        return instance.findElement(By.xpath("//td[@title='"+visibleDay+"']")); //eg: April 28, 2019
    }

    public static WebElement dateMonth(String month) {
        return instance.findElement(By.xpath("//a[contains(text(),'"+month+"')]")); //eg: Dec,Aug
    }
    public static WebElement dateYear(String year) {
        return instance.findElement(By.xpath("//div[contains(@class,'monthpicker')]/a[contains(text(),'"+year+"')]"));

    }

    public static WebElement button(String buttonName) {
        return instance.findElement(By.xpath("//span[@class='x-btn-button' and contains(.,'"+buttonName+"')]"));
    }


    public static WebElement deviceConfigurationField(String typeOfField) {
        return instance.findElement(By.xpath("//input[starts-with(@class,'x-form')][@name='"+typeOfField+"']"));
    }

    public static WebElement shipmentDate( ) {
        return instance.findElement(By.xpath("//td[contains(.,'Shipment date')]/following-sibling::td"));
    }

    public static WebElement deviceSummaryField(String field) {
        return instance.findElement(By.xpath(".//td[contains(.,'"+field+"')]/following-sibling::td/div[1]"));
    }

    public static WebElement dropdown(String typeOfField, String dropdownNumber) {
        return instance.findElement(By.xpath("(//td[label[text()='"+typeOfField+"']]/following-sibling::td[1]//td/div[contains(@class,'arrow-trigger')])["+dropdownNumber+"]"));
    }

    public static WebElement addDeviceDropdownSelection(String selection) {
        return instance.findElement(By.xpath("//li[contains(text(),'"+selection+"')]"));
    }

    public static WebElement searchButton() {
        return instance.findElement(By.xpath(".//span[text()='Search']"));
    }

    public static WebElement searchCriteria(String criteria) {
        return instance.findElement(By.xpath("//a[.='"+criteria+"']"));
    }

    public static WebElement searchCriteriaDropdownSelection(String selection) {
        return instance.findElement(By.xpath("//li[contains(text(),'"+selection+"')]"));
    }

    public static WebElement dropdownSearchCriteria() {
        return instance.findElement(By.xpath("(//div[contains(@class,'arrow-trigger')])[3]"));
    }

    public static WebElement searchCriteriaTextField() {
        return instance.findElement(By.xpath("//input[starts-with(@id,'textfield')]"));
    }

    public static WebElement searchCriteriaButton(String buttonName) {
        return instance.findElement(By.xpath("//div[contains(@id,'panel')]//a//span[text()='"+buttonName+"']"));
    }

    public static WebElement searchResult(String searchResult) {
        return instance.findElement(By.xpath("//div[contains(@id,'search-result')]//a[contains(text(),'"+searchResult+"')]"));
    }

    public static WebElement radioButton(String radioNumber) {
        return instance.findElement(By.xpath("(//input[contains(@id,'radiofield')])["+radioNumber+"]"));
    }

    public static WebElement noSearchResultsFound() {
        return instance.findElement(By.xpath("//div[contains(text(),'No search results found')]"));
    }

    public static WebElement dateOKButton() {
        return instance.findElement(By.xpath("(//a[contains(.,'OK')])[2]"));
    }

    public static WebElement dateOKButton2() {

        return instance.findElement(By.xpath("(//a[contains(.,'OK')])[1]"));
    }

    public static WebElement dateFieldButton2() {
        return instance.findElement(By.xpath("(//td[contains(@id,'datefield')]/following-sibling::td)[1]"));
    }
}

