package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class commandsPage {

    public static WebElement buttonCommands() {
        return instance.findElement(By.xpath("//span[contains(@id,'exporterbutton')]/../following-sibling::a//span[contains(text(),'Add command')]"));
    }
    public static WebElement timeField(String itemNo) {
        return instance.findElement(By.xpath("(//td[label[text()='Release date']]/following-sibling::td[1]//input)["+itemNo+"]"));
    }



}
