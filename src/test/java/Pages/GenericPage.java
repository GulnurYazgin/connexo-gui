package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class GenericPage {
    public static WebElement headerPageText(String header) {
        return instance.findElement(By.xpath(".//*[contains(text(),'"+header.trim()+"') and contains(@class,'header')]"));
    }

    public static WebElement mainMenuButton() {
        return instance.findElement(By.xpath(".//span[starts-with(@id,'uni-nav-appcenter') and contains(@id,'btnEl')]"));
    }

    public static WebElement mainSubMenu(String submenu) {
        return instance.findElement(By.xpath(".//span[@class='name'][contains(text(),'" + submenu + "')]"));
    }

    public static WebElement pageMenu(String pageMenu) {
        return instance.findElement(By.xpath(".//span[contains(text(),'" + pageMenu + "') and contains(@id, 'btn')]"));
    }

    public static WebElement pageSection(String sectionTitle, String subSection) {
        return instance.findElement(By.xpath(".//div[contains(.,'" + sectionTitle + "') and @class='x-panel x-column x-panel-tile']//a//span[contains(text(),'" + subSection + "')]"));
    }

    public static WebElement textField(String fieldName) {
        return instance.findElement(By.xpath(".//td[contains(.,'"+fieldName+"')]/following-sibling::td//input"));
    }

    public static WebElement button(String selection) {//action button, action list, next button etc
//        return instance.findElement(By.xpath("//a[contains(.,'"+selection+"')]"));
        return instance.findElement(By.xpath("//span[contains(text(),'"+selection+"')][contains(@id,'button')]"));

    }
    public static WebElement buttonSelection(String selection) {//action button, action list, next button etc
//        return instance.findElement(By.xpath("//a[contains(.,'"+selection+"')]"));
        return instance.findElement(By.xpath("//span[contains(text(),'"+selection+"')][contains(@id,'menuitem')]"));

    }

    public static WebElement popupButton(String selection) {
        return instance.findElement(By.xpath("//span[contains(@class,'x-btn-wrap')][contains(.,'"+selection+"')]"));
    }
    public static WebElement popupMessage(String message) {
        return instance.findElement(By.xpath("//label[contains(.,'"+message+"')])"));
    }
    public static WebElement sideMenuItem(String item) {
        return instance.findElement(By.xpath("//span[contains(text(),'"+item+"')]"));
    }
    public static WebElement communicationTaskName(String item) {
        return instance.findElement(By.xpath("//div[contains(text(),'"+item+"')]/../following-sibling::td[6]"));
    }
    public static WebElement communicationTaskStatus(String item) {
        return instance.findElement(By.xpath("//div[contains(text(),'"+item+"')]/../following-sibling::td[3]"));
    }
    public static WebElement itemsPerPageDropdown(String itemName) {
        return instance.findElement(By.xpath("//div[contains(text(),'"+itemName+"')]/../..//div[contains(@class,'arrow-trigger')]"));
    }
    public static WebElement registerValue(String registerName) {
        return instance.findElement(By.xpath("//div[contains(.,'"+registerName+"')]/../following-sibling::td[2]"));
    }


}

