package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class securityAccessorsPage {
    public static WebElement securityAccessorActionButton(String item) {
        return instance.findElement(By.xpath("//div[contains(text(),'"+item+"')]/../following-sibling::td[3]"));
    }

}
