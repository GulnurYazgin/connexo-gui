package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class startProcessPage {

    public static WebElement processNumber(String processName) {
        return instance.findElement(By.xpath("(.//div[contains(text(),'"+processName+"')]/../preceding-sibling::td/div)[1]"));
    }
    public static WebElement processStatus(String processNumber) {
        return instance.findElement(By.xpath("//div[contains(text(),'"+processNumber+"')]/../following-sibling::td[4]"));
    }
    public static WebElement processTab(String tabName) {
        return instance.findElement(By.xpath("//a[contains(.,'"+tabName+"')][not(contains(@id,'menuitem'))]"));
    }

    public static WebElement historyPageProcessID(String processID) {
        return instance.findElement(By.xpath("//td[contains(.,'"+processID+"')]"));
    }
    public static WebElement historyPageStatus(String processID) {
        return instance.findElement(By.xpath("(//tr[contains(.,'"+processID+"')]/td/div)[5]"));
    }

    public static WebElement historyPageApplyButton() {
        return instance.findElement(By.xpath("//a[contains(., 'Apply')]"));
    }

    public static WebElement specificProcessID(String processID) {
        return instance.findElement(By.xpath("//div[contains(text(),'"+processID+"')]/.."));
    }

}
