package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class firmwarePage {

    public static WebElement firmwareActionButton(String firmwareType) {
        return instance.findElement(By.xpath("//div[contains(.,'"+firmwareType+"')]/a[contains(.,'Action')]"));
    }
    public static WebElement firmwareVersionFieldPerFirmware(String firmwareType) {
        return instance.findElement(By.xpath("//div[contains(@id,'device-firmware-form')][contains(.,'"+firmwareType+"')]//td[contains(.,'Firmware version')]/following-sibling::td"));
    }
    public static WebElement dropdownByNumber(String dropdownNo) {
        return instance.findElement(By.xpath("(//div[contains(@class,'arrow-trigger')])["+dropdownNo+"]"));
    }
    public static WebElement firmwarePageDropdownSelection(String selection) {
        return instance.findElement(By.xpath("(//li[contains(text(),'"+selection+"')])[1]"));
    }
    public static WebElement firmwareUpgradeMessage() {
        return instance.findElement(By.xpath("//div[contains(text(),'Upload and activation of version/image')]")); // isdersan sonuna fw da ekleyebilin +firmwareVersion+" pending')]
    }
    public static WebElement firmwareUpgradeTime() {
        return instance.findElement(By.xpath("//div[contains(text(),'Pending')]/../../td[3]"));
    }
    public static WebElement firmwareUpgradeStatus(String time) {
        return instance.findElement(By.xpath("//div[contains(text(),\""+time+"\")]/../../td[5]/div"));
    }
}
