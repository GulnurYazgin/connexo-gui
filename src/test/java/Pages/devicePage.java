package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Config.driver.instance;

public class devicePage {

    public static WebElement subMenuOptions(String selection) {
        return instance.findElement(By.xpath(".//div[contains(@class,'x-box-item x-component-default')]//span[contains(.,'"+selection+"')]"));
    }
    public static WebElement overviewLink(String selection) {
        return instance.findElement(By.xpath("//a[contains(.,'"+selection+"')]"));
    }
    public static WebElement iconButton(String buttonIconName) {
//        return instance.findElement(By.xpath(".//td[contains(.,'"+selection+"')]/following-sibling::td/div[1]//a[@data-qtip="+buttonIconName+"]"));
        return instance.findElement(By.xpath("//*[@data-qtip='"+buttonIconName+"']"));
    }
    public static WebElement slaveDeviceField(String selection) {
       return instance.findElement(By.xpath("//a[contains(text(),'"+selection+"')]"));
    }
    public static WebElement MasterOrSlaveDeviceField() {
        return instance.findElement(By.xpath("(//div[contains(@id,'deviceCommunicationTopologyPanel')]//a)[1]"));
    }
    public static WebElement emptyMasterOrSlaveDeviceField() {
        return instance.findElement(By.xpath("//div[contains(@id,'deviceCommunicationTopologyPanel')]//div[contains(@role,'textbox')]"));
    }

}
