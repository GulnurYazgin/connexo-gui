package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        junit = "--step-notifications",
        strict = true,
        monochrome = true,
        tags = {"@Test"},
        features = {"classpath:Feature_Files"},
        glue = {"stepDefs","Config","Hooks"}
)

public class TestRunner {

}
