package Hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;

import static Config.Config_Connexo.getEnvironment;
import static Config.driver.*;

public class Hooks {


    @Before
    public void setup() {
        String defaultURL = "https://" + getEnvironment();// + "/apps/login/index.html"
        setDriverLocation("Chrome");
        maximize();
        setWait();
        goToUrl(defaultURL);
    }
    @After

    public void closed() {
        instance.quit();
//        System.out.println("skip");
    }

}
